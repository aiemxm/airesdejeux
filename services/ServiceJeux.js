function serviceJeux() {
  myAjax(
    "https://data.toulouse-metropole.fr/api/records/1.0/search/?dataset=aires-de-jeux&q=&rows=12&facet=nom&facet=nb_jeux",
    "",
    function (data) {
      console.log(data);
      Jeux.afficherDiv1(data);
      Jeux.afficherDiv2(data);
      Jeux.map(data);
      Jeux.carrousel(data);
    },
    function (data) {
      console.log("error");
    }
  );
}
