function myAjax(url, data, callbackSuccess, callbackError){
    $.ajax({
        url: url,
        data: data,
        async: true,
        dataType: "json",
        method: "get",
        success: function(result){
            callbackSuccess(result);
        },
        error: function(result){
            callbackError(result);
        },
        complete: function(){
            console.log('fini');
        }
    });
}