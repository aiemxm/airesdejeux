var Jeux = {
  getJeux: function () {
    serviceJeux();
  },
  //*affichage carrousel
  afficherDiv1: function (data) {
    $("body div.container div.row div:nth-of-type(1)").html(
      "<div id=title class='text-uppercase'></div>" +
        "<div class='swiper mySwiper'>" +
        "<div class='swiper-wrapper'> " +
        "<div class='swiper-slide'><img src='./composants/AireDeJeux/playground/14.jpg'></div>" +
        "<div class='swiper-slide'><img src='./composants/AireDeJeux/playground/2.png'></div>" +
        "<div class='swiper-slide'><img src='./composants/AireDeJeux/playground/3.jpg'></div>" +
        "<div class='swiper-slide'><img src='./composants/AireDeJeux/playground/4.jpg'></div>" +
        "<div class='swiper-slide'><img src='./composants/AireDeJeux/playground/5.jpg'></div>" +
        "<div class='swiper-slide'><img src='./composants/AireDeJeux/playground/6.jpg'></div>" +
        "<div class='swiper-slide'><img src='./composants/AireDeJeux/playground/7.jpg'></div>" +
        "<div class='swiper-slide'><img src='./composants/AireDeJeux/playground/8.jpeg'></div>" +
        "<div class='swiper-slide'><img src='./composants/AireDeJeux/playground/9cloture.jpg'></div>" +
        "<div class='swiper-slide'><img src='./composants/AireDeJeux/playground/10cloture2.jpg'></div>" +
        "<div class='swiper-slide'><img src='./composants/AireDeJeux/playground/11cloture3.jpg'></div>" +
        "<div class='swiper-slide'><img src='./composants/AireDeJeux/playground/12cloture4.jpg'></div>" +
        "</div>" +
        "<div class='swiper-button-next' id='next'></div>" +
        "<div class='swiper-button-prev' id='previous'></div>" +
        "<div class='swiper-pagination'></div>" +
        "</div>"
    );
    //* Carrousel
    var swiper = new Swiper(".mySwiper", {
      slidesPerView: 1,
      spaceBetween: 30,
      rewind: true,
      pagination: {
        el: ".swiper-pagination",
        clickable: true,
      },
      navigation: {
        nextEl: ".swiper-button-next",
        prevEl: ".swiper-button-prev",
      },
    });

    $(".swiper-slide").mouseover(function () {
      $(".swiper-slide").css({
        "webkit-filter": "blur(5px)",
        filter: "blur(5px)",
        transition: "0.3s ease",
      });
    });

    $(".swiper-slide").mouseout(function () {
      $(".swiper-slide").css({
        "webkit-filter": "blur(0px)",
        filter: "blur(0px)",
        transition: "0.3s ease",
      });
    });
  },
  afficherDiv2: function (data, i) {
    $("body>div.container>div.row>div:nth-of-type(2)").html(
      '<div id="map"></div>'
    );
  },

  //* création de la map
  map: function (data) {
    console.log("test");

    const coorToulouse = {
      lat: data.records[0].geometry.coordinates[1],
      lng: data.records[0].geometry.coordinates[0],
    };
    const zoomLevel = 18.0;
    const map = L.map("map").setView(
      [coorToulouse.lat, coorToulouse.lng],
      zoomLevel
    );
    L.tileLayer("https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png", {
      maxZoom: zoomLevel,
      minZoom: zoomLevel - 10,
      attribution: "© OpenStreetMap",
    }).addTo(map);

    //* creation marqueurs sur la map
    $.each(data.records, function (x, y) {
      /*   var marker = L.marker([
        y.fields.geo_point_2d[0],
        y.fields.geo_point_2d[1],
      ]);
      marker.addTo(map); */
      var popup = L.popup()
        .setLatLng([y.geometry.coordinates[1], y.geometry.coordinates[0]])
        .setContent(y.fields.nom);
      popup.addTo(map);
    });
    $("#next").click(function () {
      map.panTo(
        new L.LatLng(
          data.records[5].geometry.coordinates[1],
          data.records[5].geometry.coordinates[0]
        )
      );
    });
  },

  // carrousel: function (data) {},
};
